import java.util.concurrent.atomic.AtomicBoolean;

class Task extends Thread 
{
    private String name;
 
    public Task(String name) 
    {
        System.out.println("Creating a new task :"+name);
        this.name = name;
    }
 
    @Override
    public void run() 
    {
        try
        {
            Long calculation = (long) (Math.random() * 100);
            System.out.println("Doing a task during : " + name);
            Thread.sleep(1);
        } 
        catch (InterruptedException e) 
        {
            e.printStackTrace();
        }
    }
}