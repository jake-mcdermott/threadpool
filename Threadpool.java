import java.util.ArrayList;

public class Threadpool 
{
    private ArrayList<Task> threads;

    private Threadpool(int maxThreads) 
    {
        this.threads = new ArrayList<>();
        for (int threadIndex = 0; threadIndex < maxThreads; threadIndex++) 
        {
            Task thread = new Task("Thread " + threadIndex);
            thread.start();
            this.threads.add(thread);
        }
    }

    public static Threadpool getInstance() {
        return new Threadpool(Runtime.getRuntime().availableProcessors());
    }
}