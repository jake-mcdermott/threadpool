# README #

### What is this repository for? ###

Create a Java socket based client-server or RPC style application where the server
implements a threadpool for executing incoming requests. You must demonstrate the service
working. The service doesn't have to do anything significant, for example a simple
calculation of some kind would suffice. The server should print out details of incoming client
connections as well as returning results to the client. You can use any RPC service including
FBThrift or Protocol Buffers if you would like to get some experience using them. Links
provided in lecture slides. Maybe an interesting interview topic.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* Jake McDermott - jakemc77@yahoo.co.uk